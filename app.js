var express = require('express');
var bodyParser = require('body-parser');

var config = require('./config/config')[process.argv[2] || "dev"]
var routes = require('./server/routes');
var app = express();

var server = require("http").Server(app);

server.listen(config.port);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    return res.status(404).json({
        error: {
            title: 'Resource Is Not Found'
        },
        meta: {
            author: "Phuc Ly"
        }
    })
});

module.exports = app;