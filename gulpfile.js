var gulp = require('gulp'),
    exec = require('gulp-exec'),
    package = require('./package.json'),
    nodemon = require('gulp-nodemon');

gulp.task('nodemon-test', function(cb) {
    return nodemon({
        script: 'app.js',
        args: ["test"],
        watch: [
            "./server"
        ]
    });
})

gulp.task('nodemon', function(cb) {
    return nodemon({
        script: 'app.js',
        args: ["dev"],
        watch: [
            "./server"
        ]
    });
})

gulp.task('serve', ['nodemon'], function() {})
gulp.task('test', ['nodemon-test'], function() {})