var _ = require("lodash");
var Product = require('../model/product');
var Category = require('../model/category');
var utils = require('../common/utils');

var controller = function(router) {
    const NOT_FOUND = {
        data: null,
        meta: {
            author: "Phuc Ly"
        }
    };

    var checkCRUBody = function(req, res, next) {
        if (!req.body.data || req.body.data.type != "categories" ||
            !req.body.data.attributes) {
            return res.status(403).json(utils.error({
                message: "Wrong Resource Format"
            }))
        }

        if (!req.body.data.attributes.name) {
            return res.status(403).json(utils.error({
                message: "Category Name Is Required"
            }))
        }

        if (!req.body.data.attributes.description) {
            return res.status(403).json(utils.error({
                message: "Category Description Is Required"
            }))
        }

        return next();
    }

    router.get('/categories/:categoryId(\\d+)', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectById({
            id: req.params.categoryId
        }, function(err, category) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!category) {
                return res.json(NOT_FOUND)
            }

            return res.json({
                links: {
                    self: Category.prototype.getLink(category, baseURL)
                },
                data: {
                    type: "categories",
                    id: category.id,
                    attributes: Category.prototype.getAttributes(category, baseURL),
                    relationships: Category.prototype.getRelationships(category, baseURL)
                },
                meta: {
                    author: "Phuc Ly"
                }
            })
        })
    });

    router.get('/categories/:categoryId(\\d+)/products', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectById({
            id: req.params.categoryId
        }, function(err, category) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!category) {
                return res.status(404).json({
                    error: {
                        title: 'Resource Is Not Found'
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Product.prototype.selectByCategoryId({
                categoryId: category.id
            }, function(err, products) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                return res.json({
                    links: {
                        self: baseURL + req.originalUrl
                    },
                    data: _.map(products, function(product) {
                        return {
                            links: {
                                self: Product.prototype.getLink(product, baseURL)
                            },
                            type: "products",
                            id: product.id,
                            attributes: Product.prototype.getAttributes(product, baseURL),
                            relationships: Product.prototype.getRelationships(product, baseURL)
                        }
                    }),
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            })
        })
    });

    router.get('/categories', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectAll({}, function(err, categories) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            return res.json({
                links: {
                    self: baseURL + req.originalUrl
                },
                data: _.map(categories, function(category) {
                    return {
                        links: {
                            self: Category.prototype.getLink(category, baseURL)
                        },
                        type: "categories",
                        id: category.id,
                        attributes: Category.prototype.getAttributes(category, baseURL),
                        relationships: Category.prototype.getRelationships(category, baseURL)
                    }
                }),
                meta: {
                    author: "Phuc Ly"
                }
            })
        })
    });

    router.post('/categories', checkCRUBody, function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.insert({
            name: req.body.data.attributes.name,
            description: req.body.data.attributes.description,
        }, function(err, category) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            return res.status(201).json({
                data: {
                    links: {
                        self: Category.prototype.getLink(category, baseURL)
                    },
                    type: "categories",
                    id: category.id,
                    attributes: Category.prototype.getAttributes(category, baseURL),
                    relationships: Category.prototype.getRelationships(category, baseURL)
                },
                meta: {
                    author: "Phuc Ly"
                }
            })
        })
    });

    router.patch('/categories/:categoryId(\\d+)', checkCRUBody, function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectById({
            id: req.params.categoryId
        }, function(err, category) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!category) {
                return res.status(404).json({
                    error: {
                        title: "Category Is Not Found"
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Category.prototype.update(_.merge(category, {
                name: req.body.data.attributes.name,
                description: req.body.data.attributes.description
            }), function(err, category) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                return res.status(200).json({
                    data: {
                        links: {
                            self: Category.prototype.getLink(category, baseURL)
                        },
                        type: "categories",
                        id: category.id,
                        attributes: Category.prototype.getAttributes(category, baseURL),
                        relationships: Category.prototype.getRelationships(category, baseURL)
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            })
        })
    });

    router.delete('/categories/:categoryId(\\d+)', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectById({
            id: req.params.categoryId
        }, function(err, product) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!product) {
                return res.status(404).json({
                    error: {
                        title: "Category Is Not Found"
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Category.prototype.delete({
                id: req.params.categoryId
            }, function(err, product) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                return res.status(200).json({
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            })
        });
    });
}
module.exports = controller;