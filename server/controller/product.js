var _ = require("lodash");
var Product = require('../model/product');
var Category = require('../model/category');
var utils = require('../common/utils');

var controller = function(router) {
    const NOT_FOUND = {
        data: null,
        meta: {
            author: "Phuc Ly"
        }
    };

    var checkCRUBody = function(req, res, next) {
        if (!req.body.data || req.body.data.type != "products" ||
            !req.body.data.attributes || !req.body.data.relationships) {
            return res.status(403).json(utils.error({
                message: "Wrong Resource Format"
            }))
        }

        if (!req.body.data.attributes.name) {
            return res.status(403).json(utils.error({
                message: "Product Name Is Required"
            }))
        }

        if (!req.body.data.attributes.description) {
            return res.status(403).json(utils.error({
                message: "Product Description Is Required"
            }))
        }

        if (!req.body.data.relationships.category ||
            !req.body.data.relationships.category.data ||
            req.body.data.relationships.category.data.type != "categories" ||
            !req.body.data.relationships.category.data.id) {
            return res.status(403).json(utils.error({
                message: "Product Category Is Required"
            }))
        }

        return next();
    }

    router.get('/products/:productId(\\d+)', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Product.prototype.selectById({
            id: req.params.productId
        }, function(err, product) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!product) {
                return res.json(NOT_FOUND)
            }

            return res.json({
                links: {
                    self: Product.prototype.getLink(product, baseURL)
                },
                data: {
                    type: "products",
                    id: product.id,
                    attributes: Product.prototype.getAttributes(product, baseURL),
                    relationships: Product.prototype.getRelationships(product, baseURL)
                },
                meta: {
                    author: "Phuc Ly"
                }
            })
        })
    });

    router.get('/products/:productId(\\d+)/category', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Product.prototype.selectById({
            id: req.params.productId
        }, function(err, product) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!product) {
                return res.status(404).json({
                    error: {
                        title: 'Resource Is Not Found'
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Category.prototype.selectById({
                id: product.categoryId
            }, function(err, category) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                if (!category) {
                    return res.json(NOT_FOUND)
                }

                return res.json({
                    links: {
                        self: Category.prototype.getLink(category, baseURL),
                    },
                    data: {
                        type: "categories",
                        id: category.id,
                        attributes: Category.prototype.getAttributes(category, baseURL),
                        relationships: Category.prototype.getRelationships(category, baseURL)
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            })
        });
    });

    router.get('/products', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Product.prototype.selectAll({}, function(err, products) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            return res.json({
                links: {
                    self: baseURL + req.originalUrl
                },
                data: _.map(products, function(product) {
                    return {
                        links: {
                            self: Product.prototype.getLink(product, baseURL)
                        },
                        type: "products",
                        id: product.id,
                        attributes: Product.prototype.getAttributes(product, baseURL),
                        relationships: Product.prototype.getRelationships(product, baseURL)
                    }
                }),
                meta: {
                    author: "Phuc Ly"
                }
            })
        })
    });

    router.post('/products', checkCRUBody, function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectById({
            id: req.body.data.relationships.category.data.id
        }, function(err, category) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!category) {
                return res.status(404).json({
                    error: {
                        title: "Product Category Is Not Found"
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Product.prototype.insert({
                name: req.body.data.attributes.name,
                description: req.body.data.attributes.description,
                categoryId: category.id
            }, function(err, product) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                return res.status(201).json({
                    data: {
                        links: {
                            self: Product.prototype.getLink(product, baseURL)
                        },
                        type: "products",
                        id: product.id,
                        attributes: Product.prototype.getAttributes(product, baseURL),
                        relationships: Product.prototype.getRelationships(product, baseURL)
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            })
        })
    });

    router.patch('/products/:productId(\\d+)', checkCRUBody, function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Category.prototype.selectById({
            id: req.body.data.relationships.category.data.id
        }, function(err, category) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!category) {
                return res.status(404).json({
                    error: {
                        title: "Product Category Is Not Found"
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Product.prototype.selectById({
                id: req.params.productId
            }, function(err, product) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                if (!product) {
                    return res.status(404).json({
                        error: {
                            title: "Product Is Not Found"
                        },
                        meta: {
                            author: "Phuc Ly"
                        }
                    })
                }

                Product.prototype.update(_.merge(product, {
                    name: req.body.data.attributes.name,
                    description: req.body.data.attributes.description,
                    categoryId: category.id
                }), function(err, product) {
                    if (err) {
                        return res.status(500).json(utils.error(err))
                    }

                    return res.status(200).json({
                        data: {
                            links: {
                                self: Product.prototype.getLink(product, baseURL)
                            },
                            type: "products",
                            id: product.id,
                            attributes: Product.prototype.getAttributes(product, baseURL),
                            relationships: Product.prototype.getRelationships(product, baseURL)
                        },
                        meta: {
                            author: "Phuc Ly"
                        }
                    })
                })
            })
        })
    });

    router.delete('/products/:productId(\\d+)', function(req, res, next) {
        const baseURL = req.protocol + '://' + req.get('host');
        Product.prototype.selectById({
            id: req.params.productId
        }, function(err, product) {
            if (err) {
                return res.status(500).json(utils.error(err))
            }

            if (!product) {
                return res.status(404).json({
                    error: {
                        title: "Product Is Not Found"
                    },
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            }

            Product.prototype.delete({
                id: req.params.productId
            }, function(err, product) {
                if (err) {
                    return res.status(500).json(utils.error(err))
                }

                return res.status(200).json({
                    meta: {
                        author: "Phuc Ly"
                    }
                })
            })
        });
    })
}
module.exports = controller;