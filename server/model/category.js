var connection = require("../../config/database_connection");
var _ = require("lodash");
var utils = require('../common/utils');
var categoryMapper = utils.mapper("category");
var moment = require('moment');

var Category = function(row) {
    _.assign(this, _.mapKeys(row, function(value, key) {
        return _.camelCase(key);
    }));

    if (this.createdDatetime) {
        this.createdDatetime = moment(this.createdDatetime).format("YYYY-MM-DD HH:mm:ss");
    }

    if (this.updatedDatetime) {
        this.updatedDatetime = moment(this.updatedDatetime).format("YYYY-MM-DD HH:mm:ss");
    }
}
Category.prototype = {
    getLink: function(category, baseURL) {
        return baseURL + '/categories/' + category.id;
    },
    getAttributes: function(category) {
        return {
            name: category.name,
            description: category.description,
            createdDatetime: category.createdDatetime,
            updatedDatetime: category.updatedDatetime
        }
    },
    getRelationships: function(category, baseURL) {
        return {
            category: {
                links: {
                    related: baseURL + '/categories/' + category.id + '/products'
                }
            }
        }
    },
    selectById: function(data, done) {
        connection.query(categoryMapper.selectById, data, function(err, rows, fields) {
            if (err) {
                return done(err);
            }

            if (rows.length == 0) {
                return done(err);
            }

            done(err, new Category(rows[0]));
        });
    },
    selectAll: function(data, done) {
        connection.query(categoryMapper.selectAll, data, function(err, rows, fields) {
            if (err) {
                return done(err);
            }

            if (rows.length == 0) {
                return done(err);
            }

            done(err, _.map(rows, function(row) {
                return new Category(row);
            }));
        });
    },
    insert: function(data, done) {
        data.createdDatetime = moment().format("YYYY-MM-DD HH:mm:ss");
        data.updatedDatetime = moment().format("YYYY-MM-DD HH:mm:ss");
        connection.query(categoryMapper.insert, data, function(err, results) {
            data.id = results.insertId;
            return done(err, data);
        });
    },
    update: function(data, done) {
        data.updatedDatetime = moment().format("YYYY-MM-DD HH:mm:ss");
        connection.query(categoryMapper.update, data, function(err, results) {
            done(err, data);
        });
    },
    delete: function(data, done) {
        connection.query(categoryMapper.delete, data, function(err, results) {
            done(err, data);
        });
    }
}

module.exports = Category;