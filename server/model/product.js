var connection = require("../../config/database_connection");
var _ = require("lodash");
var utils = require('../common/utils');
var productMapper = utils.mapper("product");
var moment = require('moment');

var Product = function(row) {
    _.assign(this, _.mapKeys(row, function(value, key) {
        return _.camelCase(key);
    }));

    if (this.createdDatetime) {
        this.createdDatetime = moment(this.createdDatetime).format("YYYY-MM-DD HH:mm:ss");
    }

    if (this.updatedDatetime) {
        this.updatedDatetime = moment(this.updatedDatetime).format("YYYY-MM-DD HH:mm:ss");
    }
}
Product.prototype = {
    getLink: function(product, baseURL) {
        return baseURL + '/products/' + product.id;
    },
    getAttributes: function(product) {
        return {
            name: product.name,
            description: product.description,
            createdDatetime: product.createdDatetime,
            updatedDatetime: product.updatedDatetime
        }
    },
    getRelationships: function(product, baseURL) {
        return {
            category: {
                links: {
                    related: baseURL + '/products/' + product.id + '/category'
                }
            }
        }
    },
    selectById: function(data, done) {
        connection.query(productMapper.selectById, data, function(err, rows, fields) {
            if (err) {
                return done(err);
            }

            if (rows.length == 0) {
                return done(err);
            }

            done(err, new Product(rows[0]));
        });
    },
    selectByCategoryId: function(data, done) {
        connection.query(productMapper.selectByCategoryId, data, function(err, rows, fields) {
            if (err) {
                return done(err);
            }

            if (rows.length == 0) {
                return done(err);
            }

            done(err, _.map(rows, function(row) {
                return new Product(row);
            }));
        });
    },
    selectAll: function(data, done) {
        connection.query(productMapper.selectAll, data, function(err, rows, fields) {
            if (err) {
                return done(err);
            }

            if (rows.length == 0) {
                return done(err);
            }

            done(err, _.map(rows, function(row) {
                return new Product(row);
            }));
        });
    },
    insert: function(data, done) {
        data.createdDatetime = moment().format("YYYY-MM-DD HH:mm:ss");
        data.updatedDatetime = moment().format("YYYY-MM-DD HH:mm:ss");
        connection.query(productMapper.insert, data, function(err, results) {
            data.id = results.insertId;
            return done(err, data);
        });
    },
    update: function(data, done) {
        data.updatedDatetime = moment().format("YYYY-MM-DD HH:mm:ss");
        connection.query(productMapper.update, data, function(err, results) {
            done(err, data);
        });
    },
    delete: function(data, done) {
        connection.query(productMapper.delete, data, function(err, results) {
            done(err, data);
        });
    }
}

module.exports = Product;