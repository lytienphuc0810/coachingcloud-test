var router = require('express').Router();
require('./controller/product')(router);
require('./controller/category')(router);
module.exports = router;
