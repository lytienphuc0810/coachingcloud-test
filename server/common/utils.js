var fs = require('fs');
var moment = require('moment');
var _ = require("lodash");
var yaml = require('js-yaml');
var config = require('../../config/config')[process.argv[2] || "dev"];

var utils = {
    mapper: function(filename) {
        return yaml.safeLoad(fs.readFileSync(__dirname + '/../../sqlmap/' + filename + '.yml', 'utf8'));
    },
    error: function(err) {
        return {
            error: {
                title: err.message
            },
            meta: {
                author: "Phuc Ly"
            }
        }
    }
}

module.exports = utils;