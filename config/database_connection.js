var mysql = require('mysql');
var config = require('./config')[process.argv[2] || "dev"];

config.database.queryFormat = function(query, values) {
    if (!values) return query;
    return query.replace(/\:(\w+)/g, function(txt, key) {
        if (values.hasOwnProperty(key)) {
            return this.escape(values[key]);
        } else {
            return null;
        }
        return txt;
    }.bind(this));
};
var pool = mysql.createPool(config.database);

module.exports = pool;
